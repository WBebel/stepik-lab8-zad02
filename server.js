const { createClient } = require('redis')
const express = require('express')
const app = express()
app.use(express.json())

const client = createClient({
    url: 'redis://redis:6379',
})

client.on('error', err => console.log('Redis Client Error', err))

app.get('/', async (req, res) => {
    const { key } = req.body
    await client.connect()
    const message = await client.get(key)
    res.json({ message })
    await client.disconnect()
})

app.post('/', async (req, res) => {
    const { key, message } = req.body
    await client.connect()
    await client.set(key, message)
    await client.disconnect()
    res.json({ message: 'Data saved.' })
})

app.listen(3000, () => console.log('Server is running on port 3000'))